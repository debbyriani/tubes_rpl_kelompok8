package com.example.petpediaaaa.Adapter;

public class Reservasi {

    String namapel, nohp, nama, checkin,checkout,ras, jumlah;

    public Reservasi(String namapel, String nohp, String nama, String checkin, String checkout, String ras, String jumlah) {
        this.namapel = this.namapel;
        this.nohp = this.nohp;
        this.nama = nama;
        this.checkin = checkin;
        this.checkout = checkout;
        this.ras = ras;
        this.jumlah = jumlah;
    }

    public  String getNamapel() {return namapel;}

    public void setNamapel(String namapel) { this.namapel = namapel;}

    public  String getNohp() {return nohp;}

    public void setNohp(String nohp) { this.nohp = nohp;}

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getCheckin() {
        return checkin;
    }

    public void setCheckin(String checkin) {
        this.checkin = checkin;
    }

    public String getCheckout() {
        return checkout;
    }

    public void setCheckout(String checkout) {
        this.checkout = checkout;
    }

    public String getRas() {
        return ras;
    }

    public void setRas(String ras) {
        this.ras = ras;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }
}
