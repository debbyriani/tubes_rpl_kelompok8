package com.example.petpediaaaa.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.petpediaaaa.R;

import java.util.ArrayList;

public class AdapterReservasi extends RecyclerView.Adapter<AdapterReservasi.ViewHolder> {
    private ArrayList<Reservasi> daftarPartner;
    private Context mContext;

    public AdapterReservasi(ArrayList<Reservasi> daftarPartner, Context mContext) {
        this.daftarPartner = daftarPartner;
        this.mContext = mContext;
    }


    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AdapterReservasi.ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.card, parent, false)) {
        };
    }
    @Override
    public void onBindViewHolder(@NonNull AdapterReservasi.ViewHolder viewHolder, int i) {
Reservasi reservasi = daftarPartner.get(i);
        viewHolder.bindTo(reservasi);
    }

    @Override
    public int getItemCount() {
        return daftarPartner.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView namapel, nohp, nama, jumlah,checkin, checkout,ras;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            namapel =itemView.findViewById(R.id.txNama);
            nohp = itemView.findViewById(R.id.txNohp);
            nama = itemView.findViewById(R.id.txHewan);
            checkin = itemView.findViewById(R.id.txCheckin);
            checkout = itemView.findViewById(R.id.txCheckout);
            ras= itemView.findViewById(R.id.txRas);
            jumlah = itemView.findViewById(R.id.txJumlah);
        }

        void bindTo(Reservasi ndx){
            namapel.setText(ndx.getNamapel());
            nohp.setText(ndx.getNohp());
            nama.setText(ndx.getNama());
            checkin.setText(ndx.getCheckin());
            checkout.setText(ndx.getCheckout());
            ras.setText(ndx.getRas());
            jumlah.setText(ndx.jumlah+"");

        }

        @Override
        public void onClick(View v) {

        }
    }
}
