/*
 * Copyright (C) 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.petpediaaaa;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

/***
 * The adapter class for the RecyclerView.
 */
class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {

    // Member variables.
    private ArrayList<Home> mHomesData;
    private Context mContext;



    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView img_home;
        private TextView txt_judul;
        private String content;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            this.img_home = itemView.findViewById(R.id.img_home);
            this.txt_judul = itemView.findViewById(R.id.txt_judul);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(v.getContext(), DetailActivity.class);
                    in.putExtra("judul", txt_judul.getText().toString());
                    in.putExtra("isi", content);
                    v.getContext().startActivity(in);
                }
            });
        }


    }

    public HomeAdapter(ArrayList<Home> mSportsData) {
        this.mHomesData = mSportsData;
    }

    @NonNull
    @Override
    public HomeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cardview_home, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final HomeAdapter.ViewHolder viewHolder, int i) {
        final Home currentItem = mHomesData.get(i);

        StorageReference islandRef = FirebaseStorage.getInstance().getReference().child(currentItem.getFoto());

        Log.d("TAG",currentItem.getFoto());
        viewHolder.txt_judul.setText(currentItem.getTitle());
        viewHolder.content = currentItem.getIsi();

        final long ONE_MEGABYTE = 10 * 1024 * 1024;
        islandRef.getBytes(ONE_MEGABYTE)
                .addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Drawable d = Drawable.createFromStream(new ByteArrayInputStream(bytes), null);
                viewHolder.img_home.setImageDrawable(d);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception exception) {
                Log.d("TAGFAIL",currentItem.getFoto());
//                Toast.makeText(mContext, "YAhh FAILL"+currentItem.getFoto(), Toast.LENGTH_SHORT).show();
                viewHolder.img_home.setImageResource(currentItem.getImageResource());
            }
        }).addOnCompleteListener(new OnCompleteListener<byte[]>() {
            @Override
            public void onComplete(@NonNull Task<byte[]> task) {
                Log.d("DOWNLOAD STATUS : ", task.toString());
            }
        });


    }

    @Override
    public int getItemCount() {
        return mHomesData.size();
    }
}
