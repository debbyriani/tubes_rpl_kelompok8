package com.example.petpediaaaa;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    private TextView mTextMessage;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
//    private RecyclerView.LayoutManager layoutManager;
    private Context mContext;
    ArrayList<Home> homes;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        homes= new ArrayList<>();
//        homes.add(new Home("Berita tentang ",R.drawable.anjing,"Isi pesannya yaa hehehehe"));
//        homes.add(new Home("Berita tentang ",R.drawable.anjing,"Isi pesannya yaa hehehehe"));
//        homes.add(new Home("Berita tentang ",R.drawable.anjing,"Isi pesannya yaa hehehehe"));

        init();

        //RecyclerView
        recyclerView = view.findViewById(R.id.recyclerViewMain);
        recyclerView.setHasFixedSize(true);
        adapter = new HomeAdapter(homes);

        recyclerView.setLayoutManager(new GridLayoutManager(mContext,getResources().getInteger(R.integer.grid_column_count)));
        recyclerView.setAdapter(adapter);

        return view;
    }

    public void init(){
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("news");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.d("DATASNAP",dataSnapshot.getValue().toString());
                if (dataSnapshot.exists()){
//                    homes.clear();
                    for (DataSnapshot snap : dataSnapshot.getChildren()){
                        if (snap.child("images").exists()){
                            homes.add(new Home(
                                    snap.child("title").getValue().toString(),
                                    snap.child("images").getValue().toString(),
                                    snap.child("isi").getValue().toString()
                            ));
                        }else{
                            homes.add(new Home(
                                    snap.child("title").getValue().toString(),
                                    snap.child("isi").getValue().toString()
                            ));
                        }
                    }
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
