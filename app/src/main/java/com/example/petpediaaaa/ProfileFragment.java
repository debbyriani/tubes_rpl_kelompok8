package com.example.petpediaaaa;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.petpediaaaa.Adapter.AdapterReservasi;
import com.example.petpediaaaa.Adapter.Reservasi;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {
    public static TextView name;
    ImageView imageView;
    FirebaseAuth mAuth;
    RecyclerView recyclerView;
    ArrayList<Reservasi> adaReservasis;
    AdapterReservasi adapterReservasi;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        name = view.findViewById(R.id.txName);
        imageView = view.findViewById(R.id.imageView3);
        mAuth = FirebaseAuth.getInstance();
        recyclerView = view.findViewById(R.id.rvReservasi);
        adaReservasis = new ArrayList<>();
        adapterReservasi = new AdapterReservasi(adaReservasis, view.getContext());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapterReservasi);
        if (mAuth.getCurrentUser() != null) {
            name.setText(mAuth.getCurrentUser().getDisplayName());
            Glide.with(this).load(mAuth.getCurrentUser().getPhotoUrl()).into(imageView);
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("reservasi").child(mAuth.getCurrentUser().getUid());
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        adaReservasis.clear();
                        for (DataSnapshot x : dataSnapshot.getChildren()) {
                            if (x.child("jumlah").exists()){
                                Log.d("UHUY",x.getValue().toString());
                                adaReservasis.add(new  Reservasi(
                                        x.child("nama").getValue().toString(),
                                        x.child("nohp").getValue().toString(),
                                        x.child("hewan").getValue().toString(),
                                        x.child("checkin").getValue().toString(),
                                        x.child("checkout").getValue().toString(),
                                        x.child("ras").getValue().toString(),
                                        x.child("jumlah").getValue().toString()));
                            }
                        }
                        adapterReservasi.notifyDataSetChanged();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }

        return view;
    }

}
