package com.example.petpediaaaa;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class HotelActivity extends AppCompatActivity {

    private Button btAlert;
    private SimpleDateFormat dateFormatter;
    private DatePickerDialog datePickerDialog;
    private TextView tvDateResult;
    private EditText btDatePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotelhewan);
        //menggunakan format tanggal dd-MM-yyyy
        //misalnya 01-12-2019
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        tvDateResult = (TextView) findViewById(R.id.editText_checkin);
        btDatePicker = (EditText) findViewById(R.id.editText_checkin);

        tvDateResult = (TextView) findViewById(R.id.editText_checkout);
        btDatePicker = (EditText) findViewById(R.id.editText_checkout);
        btAlert = (Button) findViewById(R.id.lakukan_reservasi);

        btAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });
    }

    private void showDateDialog(){
        //Calendar untuk mendapatkan tanggal sekarang
        Calendar newCalendar = Calendar.getInstance();
        //Inisiasi dialog DatePicker
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener(){
            @Override
            public void onDateSet(DatePicker view, int year, int montOfYear, int dayOfMont){
                //method ini dipanggil saat kita selesai memilih tanggal di DatePicker
                //Set Calendar untuk menampung tanggal yang dipilih
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, montOfYear, dayOfMont);
                //update EditText dengan tanggal yang kita pilih
                tvDateResult.setText(dateFormatter.format(newDate.getTime()));
            }
        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    private void showDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
    //set title dialognya
        alertDialogBuilder.setTitle("Terimakasih");
    //set pesan dari dialog
        alertDialogBuilder
        .setMessage("Reservasi telah dilakukan, silahkan tunggu " +
                "konfirmasi selanjutnya melalui email anda")
        .setIcon(R.mipmap.ic_launcher)
        .setCancelable(false)
        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //jika timbol diklik, maka akan menutup activity ini
                dialog.cancel();
            }
        });
        //membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();
        //menampilkan alert dialog
        alertDialog.show();
    }
}
